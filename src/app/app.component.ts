import { Component, ViewChild } from '@angular/core';
import { Papa } from 'ngx-papaparse';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngxpapaparse-angular';
  csvRecords: any[] = [];
  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;


  constructor(private papa: Papa) { }

  // Your applications input change listener for the CSV File
  fileChangeListener($event: any): void {

    // Select the files from the event
    const files = $event.srcElement.files;
    
    let i = 0;
    //Chunk size you specify decides no.of records to load at a time, this gets shown in chunk function
    this.papa.setLocalChunkSize(5000);
    this.papa.parse(files[0],{
        // step: (result) =>{
        //   i=i+1;
        //   console.log('Step: '+i, result);
        // },
        chunk: (result) =>{
          i=i+1;
          console.log('Chunk: '+ i +'\n Records: '+result.data.length+'\n Data :', result);
        },
        // complete: (result) => {
        //     //This wont return data of step function is used
        //     console.log('Parsed: ', result);
        // }
    });

    console.log('Triggered');
  }
}
